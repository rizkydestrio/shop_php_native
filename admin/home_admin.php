<?php
session_start();

if(!isset($_SESSION['username'])){
	header("location:index.php?error2");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Paweł 'kilab' Balicki - kilab.pl" />
<title>PROGRESS - Bussiness Company</title>
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/navi.css" media="screen" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".box .h_title").not(this).next("ul").hide("normal");
	$(".box .h_title").not(this).next("#home").show("normal");
	$(".box").children(".h_title").click( function() { $(this).next("ul").slideToggle(); });
});
</script>
<style>

	.hide{ display:none !important;  /*visibility:hidden;*/ }

</style>
</head>
<body>
<div class="wrap">
	<div id="header">
		<div id="top">
			<div class="left">
				<p>Welcome, <strong><?php echo $_SESSION['username'];?></strong> [ <a href="process/submit_logout.php">logout</a> ]</p>
			</div>
			<div class="right">
				<div class="align-right">
					<p><strong></strong></p>
				</div>
			</div>
		</div>
		<div id="nav"> <?php #ini adalah menu ?>
			<ul>
				<li class="upp"><a href="#">News</a>
				</li>
				<li class="upp "><a href="#">Member</a>
					<ul class="hide">
						<li>&#8250; <a href="">Show all pages</a></li>
						<li>&#8250; <a href="">Add new page</a></li>
						<li>&#8250; <a href="">Add new gallery</a></li>
						<li>&#8250; <a href="">Categories</a></li>
					</ul>
				</li>
				<li class="upp "><a href="#">User</a>
					<ul class="hide">
						<li>&#8250; <a href="">Show all uses</a></li>
						<li>&#8250; <a href="">Add new user</a></li>
						<li>&#8250; <a href="">Lock users</a></li>
					</ul>
				</li>
				<li class="upp "><a href="home_admin.php">Product</a>
					<ul class="hide">
						<li>&#8250; <a href="">Site configuration</a></li>
						<li>&#8250; <a href="">Contact Form</a></li>
					</ul>
				</li>
        <li class="upp "><a href="#">Contact</a>
				 	<ul class="hide">
						<li>&#8250; <a href="">Show all uses</a></li>
						<li>&#8250; <a href="">Add new user</a></li>
						<li>&#8250; <a href="">Lock users</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div id="content">
		<div id="sidebar">
			<div class="box">
				<div class="h_title">&#8250; Menu Content</div>
				<ul id="home">
					<li class="b1"><a class="icon page" href="news_admin.php">News </a></li>
          <li class="b1"><a class="icon add_user" href="member_admin.php">Member</a></li>
			 		<li class="b2"><a class="icon config" href="user_admin.php">User</a></li>
					<li class="b2"><a class="icon product" href="home_admin.php">Product</a></li>
          <li class="b2"><a class="icon contact" href="contact_admin.php">Contact</a></li>
				</ul>
			</div>
			<div class="box hide">
				<div class="h_title">&#8250; Manage content</div>
				<ul>
					<li class="b1"><a class="icon page" href="">Show all pages</a></li>
					<li class="b2"><a class="icon add_page" href="">Add new page</a></li>
					<li class="b1"><a class="icon photo" href="">Add new gallery</a></li>
					<li class="b2"><a class="icon category" href="">Categories</a></li>
				</ul>
			</div>
			<div class="box hide">
				<div class="h_title">&#8250; Users</div>
				<ul>
					<li class="b1"><a class="icon users" href="">Show all users</a></li>
					<li class="b2"><a class="icon add_user" href="">Add new user</a></li>
					<li class="b1"><a class="icon block_users" href="">Lock users</a></li>
				</ul>
			</div>
			<div class="box hide">
				<div class="h_title">&#8250; Settings</div>
				<ul>
					<li class="b1"><a class="icon config" href="">Site configuration</a></li>
					<li class="b2"><a class="icon contact" href="">Contact Form</a></li>
				</ul>
			</div>
		</div>
		<div id="main">
		  <div class="clear"></div>
		  <div class="full_w">
		    <div class="h_title">Manage pages - table</div>
		    <h2>Product</h2><?php //judul table ?>
		    <p>Ini adalah Content Produk, untuk menambah produk baru, silahkan menginput produk pada Form dibawah. <br>
		       Untuk edit produk, silahkan klik tombol edit pada list produk, dan edit form dibawah.
		    </p>
		    <div class="entry">
		      <div class="sep"></div>
		    </div>
		    <p>
		      <a href="home_admin.php?create">
		         <input type="button" value="Add Product" style="padding:5px;background:#333;color:#DACDCD;">
		      </a>
		    </p>
		    <table>
		      <thead>
		        <tr>
		          <th scope="col">Id Product</th>
		          <th scope="col">Image</th>
		          <th scope="col">Name</th>
		          <th scope="col">Price</th>
		          <th scope="col">Stock</th>
		          <th scope="col">Description</th>
		          <th scope="col" style="width: 65px;">Modify</th>
		        </tr>
		      </thead>
		      <tbody>
		      <?php
		      //include "../config/config.php";
		      include "../config/config.php";

		      $rowperpage = "5";
		      if(isset($_GET['page'])){
		         $pagenow = $_GET['page'];
		      }else{
		        $pagenow = "1";
		      }
		      $startrow = ($pagenow - 1) * $rowperpage;
		      $sql = "SELECT * from product_tbl ORDER BY product_id desc LIMIT $startrow,$rowperpage";
		      $qsql = $connect->query($sql);
		      $rowproduct = $qsql->fetch_assoc();

		      do {
		      ?>
		        <tr>
		          <td class="align-center"><?php echo $rowproduct['product_id']; ?></td>
		          <td><img src="../images/product/<?php echo $rowproduct['gambar']; ?>" width="70" height="70" /></td>
		          <td><?php echo $rowproduct['product_name']; ?></td>
		          <td><?php echo number_format($rowproduct['price']); ?></td>
		          <td><?php echo $rowproduct['stock']; ?></td>
		          <td><?php echo substr($rowproduct['product_description'],0,100); ?></td>
		          <td>
		            <a href="home_admin.php?edit&id=<?php echo $rowproduct['product_id']; ?>" class="table-icon edit" title="Edit"></a>
		            <a href="#" class="table-icon archive" title=" View Comment"></a>
		            <a href="process/delete_product.php?id=<?php echo $rowproduct['product_id']; ?>" class="table-icon delete" title="Delete" onclick="return confirm('apakah anda yakin data ber id=<?php echo $rowproduct['product_id']; ?> ingin dihapus ?') "></a>
		          </td>
		        </tr>
		      <?php }while($rowproduct = $qsql->fetch_assoc()); ?>
		      </tbody>
		    </table>
		    <div class="entry">
		      <?php
		       $sqltotal = "SELECT * from product_tbl";
		       $qtotal = $connect->query($sqltotal);
		       $total_data = $qtotal->num_rows;

		       // Tentukan berapa halaman yang didapat //
		       $total_page = ceil($total_data/$rowperpage);

		       // Tampilkan paging2 yang didapat dengan looping //
		      ?>
		      <div class="pagination">
		      <?php
		      for($i=1;$i<=$total_page;$i++){
		        if($i == $pagenow){ ?>
		            <span class="this_page">
		               <?php echo $i; ?>
		            </span>
		        <?php
		        }else{ ?>
		            <a class="page" href="?page=<?php echo $i; ?>">
		                <?php echo $i; ?>
		            </a>
		      <?php
		        }
		      }
		      ?>
		      </div>
		      <div class="sep hide"></div>
		      <a class="button add hide" href="">Add new page</a> <a class="button hide" href="">Categories</a>
		    </div>
		  </div>
		  <div class="full_w" id="form_geje">
		  <?php
		  if(isset($_GET['edit'])){
		  // --- SAAT KLIK EDIT MASUK KESINI (JADI FORM EDIT)--- //
		    $productid = $_GET['id'];
		    $sqledit = "SELECT * from product_tbl where product_id = '$productid'";
		    $qsql = $connect->query($sqledit);
		    $rowedit = $qsql->fetch_assoc();

		    $name   = $rowedit['product_name'];
		    $brand  = $rowedit['brand'];
		    $price  = $rowedit['price'];
		    $stock  = $rowedit['stock'];
		    $images = $rowedit['gambar'];
		    $desc   = $rowedit['product_description'];
		    $action = "process/edit_product.php";
		    $imagesfrm = "<img src='../images/product/$images'; width='200' height='200'/>";
		    $htitle = "Edit Product : <b> $name </b>";
		  }else{
		  // --- DEFAULT / WAKTU KLIK ADD PRODUK MASUK KESINI (FORM CREATE PRODUK)-- //
		    $name   = "";  $brand  = "";   $price  = "";
		    $stock  = "";  $images = "";   $desc   = "";
		    $action = "process/create_product.php";
		    $imagesfrm = "";  $htitle = "Add Product";
		  }
		  ?>
		  <div class="h_title"><?php echo $htitle; ?></div>
		  <div>
		      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
		      <input type="hidden" name="productid" value="<?php echo $productid; ?>" />
		      <div class="element" >
		        <label for="name"> Product Name <span class="red">(required)</span></label>
		        <input id="name" name="name" class="text <?php //err ?>"
		        value="<?php echo $name; ?>" required />
		      </div>
		      <div class="element" >
		        <label for="brand"> Brand <span class="red">(required)</span></label>
		        <input id="brand" name="brand" class="text <?php //err ?>"
		        value="<?php echo $brand; ?>" required />
		      </div>
		      <div class="element" >
		        <label for="price"> Price <span class="red">(required)</span></label>
		        <input id="price" name="price" class="text <?php //err ?>"
		        value="<?php echo $price; ?>" required />
		      </div>
		      <div class="element" >
		        <label for="stock"> Stock <span class="red">(required)</span></label>
		        <input id="stock" name="stock" class="text <?php //err ?>"
		        value="<?php echo $stock; ?>" required />
		      </div>
		      <div class="element">
		        <label for="description"> Description <span class="red">(required)</span></label>
		        <textarea class="textarea" name="description" rows="10" required><?php echo $desc; ?></textarea>
		      </div>
		      <div class="element">
		        <?php echo $imagesfrm; ?>
		        <input type="hidden" name="old_images" value="<?php echo $images; ?>" />
		        <label for="gambar">Attachments</label>
		        <input type="file" name="gambar" />
		      </div>
		      <div class="element">
		      </div>
		      <div class="entry">
		         <button type="submit" class="add">Save </button> <button type="reset" class="cancel">Reset</button>
		      </div>
		    </form>
		  </div>
		</div>
		<div class="clear"></div>
		</div>
	<div id="footer">
		<div class="left">
			<p>Design: <a href="http://kilab.pl">Paweł Balicki</a> | Admin Panel: <a href="">YourSite.com</a></p>
		</div>
		<div class="right">
			<p><a href="">Example link 1</a> | <a href="">Example link 2</a></p>
		</div>
	</div>
</div>

</body>
</html>
