<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Station Shop Theme - Products Page</title>
<meta name="keywords" content="station shop, products, theme, website templates, CSS, HTML" />
<meta name="description" content="Station Shop Products - free CSS template by templatemo.com" />
<link href="css/templatemo_style.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "top_nav", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>

<link rel="stylesheet" type="text/css" media="all" href="css/jquery.dualSlider.0.2.css" />

<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js/jquery.timers-1.2.js" type="text/javascript"></script>

</head>

<body>

<div id="templatemo_wrapper">
    <?php
    // ---- HEADER & MENU HERE ---- //
    include "view/header_menu.php";
    ?>

    <div id="templatemo_main">
   		<div id="sidebar" class="float_l">
        	<div class="sidebar_box"><span class="bottom"></span>
            	<h3>Categories<a href="http://www.onlyimage.com" title="only image" class="more_link"  target="_blank"></a></h3>
                <div class="content">
                	<ul class="sidebar_list">
                    	<li class="first"><a href="#">Aenean varius nulla</a></li>
                        <li><a href="#">Cras mattis arcu</a></li>
                        <li><a href="#">Donec turpis ipsum</a></li>
                        <li><a href="#">Fusce sodales mattis</a></li>
                        <li><a href="#">Maecenas et mauris</a></li>
                        <li><a href="#">Mauris nulla tortor</a></li>
                        <li><a href="#">Nulla odio ipsum</a></li>
                        <li><a href="#">Nunc ac viverra nibh</a></li>
                        <li><a href="#">Praesent id venenatis</a></li>
                        <li><a href="#">Quisque odio velit</a></li>
                        <li><a href="#">Suspendisse posuere</a></li>
                        <li><a href="#">Tempus lacus risus</a></li>
                        <li><a href="#">Ut tincidunt imperdiet</a></li>
                        <li><a href="#">Vestibulum eleifend</a></li>
                        <li class="last"><a href="#">Velit mi rutrum diam</a></li>
                    </ul>
                </div>
            </div>
            <div class="sidebar_box"><span class="bottom"></span>
            	<h3>Best Sellers <a href="http://cz.onlyimage.com/free-images/sign" title="Znamení obrázek" class="more_link"  target="_blank"></a></h3>
                <div class="content">
                	<div class="bs_box">
                    	<a href="#"><img src="images/templatemo_image_01.jpg" alt="Image 01" /></a>
                        <h4><a href="#">Donec nunc nisl</a></h4>
                        <p class="price">$10</p>
                        <div class="cleaner"></div>
                    </div>
                    <div class="bs_box">
                    	<a href="#"><img src="images/templatemo_image_01.jpg" alt="Image 02" /></a>
                        <h4><a href="#">Aenean eu tellus</a></h4>
                        <p class="price">$12</p>
                        <div class="cleaner"></div>
                    </div>
                    <div class="bs_box">
                    	<a href="#"><img src="images/templatemo_image_01.jpg" alt="Image 03" /></a>
                        <h4><a href="#">Phasellus ut dui</a></h4>
                        <p class="price">$20</p>
                        <div class="cleaner"></div>
                    </div>
                    <div class="bs_box">
                    	<a href="#"><img src="images/templatemo_image_01.jpg" alt="Image 04" /></a>
                        <h4><a href="#">Vestibulum ante</a></h4>
                        <p class="price">$16</p>
                        <div class="cleaner"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="content" class="float_r">
        	<h1>New Products</h1>
            <?php
            include "config/config.php";

            $rowperpage = "6";
            if(isset($_GET['page'])){
               $pagenow = $_GET['page'];
            }else{
              $pagenow = "1";
            }
            $startrow = ($pagenow - 1) * $rowperpage;
            $sqlproduct = "SELECT * from product_tbl ORDER BY product_id desc LIMIT $startrow,$rowperpage";
            $qsql = $connect->query($sqlproduct);
            $rowproduct = $qsql->fetch_assoc();

            $i = 0;
            do {
              $i = $i+1; // UNTUK TIAP LOOPING DATA KELIPATAN 3, DITAMBAHKAN CLASS NO_MARGIN_RIGHT //
            ?>
            <div class="product_box <?php if(($i % 3) == 0){echo "no_margin_right";}?>">
            	<a href="product_detail.php?id=<?php echo $rowproduct['product_id'];?>"><img src="images/product/<?php echo $rowproduct['gambar'];?>" alt="images <?php echo $rowproduct['product_id'];?>" /></a>
                <h3><?php echo $rowproduct['product_name'];?></h3>
                <p class="product_price">$ <?php echo $rowproduct['price'];?></p>
                <a href="shoppingcart.php" class="add_to_card">Add to Cart</a>
                <a href="product_detail.php?id=<?php echo $rowproduct['product_id'];?>" class="detail">Detail</a>
            </div>
          <?php }while($rowproduct = $qsql->fetch_assoc()); ?>
        </div>
        <div id="content" class="float_r">
             <?php
              $sqltotal = "SELECT * from product_tbl";
              $qtotal = $connect->query($sqltotal);
              $total_data = $qtotal->num_rows;

              // Tentukan berapa halaman yang didapat //
              $total_page = ceil($total_data/$rowperpage);

              // Tampilkan paging2 yang didapat dengan looping //
             ?>
             <div class="paging">
             <?php
             for($i=1;$i<=$total_page;$i++){
               if($i == $pagenow){ ?>
                   <span class="this_page_prd" style="padding:5px;">
                      <?php echo $i; ?>
                   </span>
               <?php
               }else{ ?>
                   <a class="page_prd" href="?page=<?php echo $i; ?>" style="padding:5px;">
                       <?php echo $i; ?>
                   </a>
             <?php
               }
             }
             ?>
             </div>
          </div>
        </div>
        <div class="cleaner"></div>
    </div> <!-- END of templatemo_main -->

    <div id="templatemo_footer">
    	<p>
			<a href="index.html">Home</a> | <a href="products.html">Products</a> | <a href="about.html">About</a> | <a href="faqs.html">FAQs</a> | <a href="checkout.html">Checkout</a> | <a href="contact.html">Contact</a>
		</p>

    	Copyright © 2048 <a href="#">Your Company Name</a> | Designed by <a href="http://www.templatemo.com" rel="nofollow" target="_parent">Free CSS Templates</a>
    </div> <!-- END of templatemo_footer -->

</div> <!-- END of templatemo_wrapper -->


<script type='text/javascript' src='js/logging.js'></script>
</body>
</html>
