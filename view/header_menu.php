<div id="templatemo_header">
  <div id="site_title">
     <h1><a href="http://www.templatemo.com" rel="nofollow">Free CSS Templates</a></h1>
  </div>
  <div id="header_right">
     <a href="#">My Account</a> | <a href="#">My Wishlist</a> | <a href="#">My Cart</a> | <a href="#">Checkout</a> | <a href="#">Log In</a>
  </div>
  <div class="cleaner"></div>
  </div> <!-- END of templatemo_header -->
  <div id="templatemo_menu">
    <div id="top_nav" class="ddsmoothmenu">
          <ul>
              <li><a href="index.php" class="selected">Home</a></li>
              <li><a href="products.php">Products</a>
                  <ul>
                      <li><a rel="nofollow" href="http://www.templatemo.com/page/1">Sub menu 1</a></li>
                      <li><a rel="nofollow" href="http://www.templatemo.com/page/2">Sub menu 2</a></li>
                      <li><a rel="nofollow" href="http://www.templatemo.com/page/3">Sub menu 3</a></li>
                </ul>
              </li>
              <li><a href="about.php">About</a>
                  <ul>
                      <li><a rel="nofollow" href="http://www.templatemo.com/page/1">Sub menu 1</a></li>
                      <li><a rel="nofollow" href="http://www.templatemo.com/page/2">Sub menu 2</a></li>
                      <li><a rel="nofollow" href="http://www.templatemo.com/page/3">Sub menu 3</a></li>
                      <li><a rel="nofollow" href="http://www.templatemo.com/page/4">Sub menu 4</a></li>
                      <li><a rel="nofollow" href="http://www.templatemo.com/page/5">Sub menu 5</a></li>
                </ul>
              </li>
              <li><a href="faqs.php">FAQs</a></li>
              <li><a href="checkout.php">Checkout</a></li>
              <li><a href="contact.php">Contact</a></li>
          </ul>
          <br style="clear: left" />
      </div> <!-- end of ddsmoothmenu -->
      <div id="menu_second_bar">
        <div id="top_shopping_cart">
            Shopping Cart: <strong>3 Products</strong> ( <a href="#">Show Cart</a> )
          </div>
        <div id="templatemo_search">
              <form action="search_product.php" method="get">
                <input type="text" value="Search" name="keyword" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" />
                <input type="submit" name="Search" value=" Search " alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
              </form>
          </div>
          <div class="cleaner"></div>
    </div>
  </div> <!-- END of templatemo_menu -->
